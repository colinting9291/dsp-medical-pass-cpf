package com.etiqa.medicalpass.dao;

import java.security.MessageDigest;

public class EBPGSecureHash {
	// String secureHash = reqSecureHashValue(hashKey, mid, invno,
	// amt).toUpperCase();

	public static String reqSignatureValue(String hashKey, String merchant_acc_no, String merchant_tranID,
			String amount) {

		String hashValues = new StringBuffer(hashKey).append(merchant_acc_no).append(merchant_tranID).append(amount)
				.toString();

		String txnSignature = SHA512(hashValues);
		return txnSignature;
	}

	public static String signResData(String hashKey, String merchant_acc_no, String merchant_tranID, String amount,
			String transaction_id, String txn_status, String response_code) {

		String hashValues = new StringBuffer(hashKey).append(merchant_acc_no).append(merchant_tranID).append(amount)
				.append(transaction_id).append(txn_status).append(response_code).toString();

		String txnSignature = SHA512(hashValues);

		return txnSignature;
	}

	public static String SHA512(String base) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuilder hexString = new StringBuilder();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if (hex.length() == 1) {
					hexString.append('0');
				}
				hexString.append(hex);
			}

			return hexString.toString();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public static String leftPaddedString(String str, Integer requiredLength, char appendVal) {
		String result = "";
		StringBuilder sb = new StringBuilder();

		for (int toPrepend = requiredLength - str.length(); toPrepend > 0; toPrepend--) {
			sb.append(appendVal);
		}

		sb.append(str);
		result = sb.toString();

		return result;
	}

}
