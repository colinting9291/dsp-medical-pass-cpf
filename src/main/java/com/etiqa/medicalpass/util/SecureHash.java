package com.etiqa.medicalpass.util;

import java.security.MessageDigest;

/**
 * This class provides applications the functionality to convert a string into a
 * hexadecimal hash value.
 *
 * @author John Diew
 * @version 1.0
 * @since 2015-01-13
 *
 */

public class SecureHash {

	// This is an array for creating hex chars
	static final char[] HEX_TABLE = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
			'E', 'F' };

	/**
	 * This method performs an arrangement with the byte array data, convert it into
	 * hexadecimal format and returns the final string.
	 *
	 * @param input
	 *            - the byte array of the initial string
	 * @return the string for the resulting hash value
	 * @author john.diew
	 *
	 */

	static String hex(byte[] input) {
		// create a StringBuffer 2x the size of the hash array
		StringBuffer sb = new StringBuffer(input.length * 2);

		// retrieve the byte array data, convert it to hex and add it to the
		// StringBuffer
		for (int i = 0; i < input.length; i++) {
			sb.append(HEX_TABLE[(input[i] >> 4) & 0xf]);
			sb.append(HEX_TABLE[input[i] & 0xf]);
		}

		return sb.toString();

	}

	/**
	 * This method will return a string of hash value using the original.
	 *
	 * @param originalString
	 *            - the original string that will be be used to produce the final
	 *            hash
	 * 
	 * @return the string for the resulting hash value
	 * @author john.diew
	 *
	 */

	public static String genSecureHash(String originalString) {
		MessageDigest md = null;
		byte[] ba = null;

		// create the md hash and ISO-8859-1 encode it
		try {
			md = MessageDigest.getInstance("SHA-256");
			ba = md.digest(originalString.getBytes("ISO-8859-1"));
		} catch (Exception e) {
		} // wont happen

		return hex(ba);

	}
}