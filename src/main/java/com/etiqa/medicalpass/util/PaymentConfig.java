package com.etiqa.medicalpass.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.etiqa.medicalpass.db.DbUtil;

import oracle.jdbc.OracleTypes;

public class PaymentConfig {

	public Map<String, String> GetPaymentConfig() {

		HashMap<String, String> paymentConfig = new HashMap<String, String>();

		Connection connection = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		DbUtil database = new DbUtil();

		try {

			connection = database.getConnection();

			cstmt = connection.prepareCall("{call DSP_PAYMENT_CONFIG_FETCH(?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);

			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(1);

			// print the results

			while (rs.next()) {

				paymentConfig.put(rs.getString("PARAM_NAME"), rs.getString("PARAM_VALUE"));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(cstmt);
			DbUtil.close(connection);
			DbUtil.close(rs);
		}

		return paymentConfig;

	}

}
