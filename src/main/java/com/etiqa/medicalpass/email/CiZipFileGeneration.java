package com.etiqa.medicalpass.email;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.etiqa.medicalpass.pojo.DspCommonTblCustomer;
import com.etiqa.medicalpass.pojo.DspMpTblQQ;
import com.etiqa.medicalpass.pojo.GeneratingReportsResponseVo;
import com.etiqa.medicalpass.service.ProductMailTemplateService;
import com.etiqa.medicalpass.util.InputOutputStreamUtil;

public class CiZipFileGeneration {
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
	private static Properties activeDirMailProp = new Properties();
	ProductMailTemplateService productTemplate = null;

	private static final Logger logger = LoggerFactory.getLogger(CiZipFileGeneration.class);

	public File zipFilesGen(DspMpTblQQ mpQqData, String policyNumber, DspCommonTblCustomer dspCommonTblCustomer,
			List<GeneratingReportsResponseVo> reportVos) {
		FileOutputStream fos = null;
		ZipOutputStream zipOut = null;
		FileInputStream fis = null;
		String statusOfZipGen = null;
		String zipFileName = policyNumber + ".zip";
		File statusOfZipGenPswd = null;

		try (InputStream in = this.getClass().getClassLoader().getResourceAsStream("application.properties")) {
			Properties prop = new Properties();
			prop.load(in);

			fos = new FileOutputStream(prop.getProperty("DocumentDestionationPath") + zipFileName);
			zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
			for (GeneratingReportsResponseVo fileName : reportVos) {
				String filename = fileName.getFileName();
				logger.info("File Name in Zip class: {}", filename);
				File input = new File(prop.getProperty("DocumentSourcePath") + filename);

				fis = new FileInputStream(input);
				ZipEntry ze = new ZipEntry(input.getName());
				zipOut.putNextEntry(ze);
				byte[] tmp = new byte[4 * 1024];
				int size = 0;
				while ((size = fis.read(tmp)) != -1) {
					zipOut.write(tmp, 0, size);
				}
				zipOut.flush();
				fis.close();
			}
			zipOut.close();
			logger.info("Done... Zipped the files...");
			statusOfZipGen = "successfully gen zip file";

			callPasswordmail(dspCommonTblCustomer.getCustomerName(), dspCommonTblCustomer.getCustomerEmail(), "N");

		} catch (FileNotFoundException ex) {
			logger.error(ex.getMessage(), ex);
			statusOfZipGen = "FileNotFoundException";
		} catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
			statusOfZipGen = "IOException";
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}
		}
		createOriginalFilesAgainAfterEmail(reportVos);
		return statusOfZipGenPswd;
	}

	private void createOriginalFilesAgainAfterEmail(List<GeneratingReportsResponseVo> FileNamelist) {

		try (InputStream in = this.getClass().getClassLoader().getResourceAsStream("application.properties")) {
			Properties prop = new Properties();
			prop.load(in);
			for (GeneratingReportsResponseVo fileName : FileNamelist) {
				String filename = fileName.getFileName();
				logger.info("File Name in Zip class: {}", filename);
				String copyfileName = filename;
				copyfileName = copyfileName.replaceAll(".pdf", "2.pdf");
				logger.info("File Name copyfileName: {}", copyfileName);

				File orginalInput = new File(prop.getProperty("DocumentSourcePath") + filename);
				File copyInput = new File(prop.getProperty("DocumentSourcePath") + copyfileName);
			}
		} catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
		}
	}

	private static void copyFileUsingStream(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			InputOutputStreamUtil.close(is);
			InputOutputStreamUtil.close(os);
		}
	}

	public boolean deleteDirectory(File theDir) {
		if (theDir.exists()) {
			File[] files = theDir.listFiles();
			if (null != files) {
				for (int i = 0; i < files.length; i++) {
					if (files[i].isDirectory()) {
						deleteDirectory(files[i]);
					} else {
						logger.info("files[i] name is" + files[i].getName());

						logger.info("files[i] path dir is" + files[i].getAbsolutePath());
						files[i].delete();
					}
				}
			}
		}
		return theDir.delete();

	}

	private void callPasswordmail(String name, String emailtosend, String pollicypassword) {
		try {
			loadEmailProps();
			String message = "";
			message += "<b>Dear   " + name + ",</b><br><br><br>";
			message += "Congratulations! Your transaction is successful. We are pleased to extend our warmest welcome to you for being a part of our family.<br><br>";

			message += "We will send another email with compressed attachment file. <br><br>";
			message += "Please use password to open your e-Documents. Your password is your date of birth in the ddmmyyyy format combined with last 4 the digits of you NRIC.<br><br>";
			message += "Example: If your Date of Birth is 06-09-1986 (06-Sep-1986) <br><br>";
			message += "                          and your NRIC is : 860906-51-2345<br><br>";
			message += "<b>Then your password is: 060919865365</b><br><br>";

			message += "Note: This is an auto email notification. Please do not reply this email. You may call our Etiqa Oneline call centre at 1300 13 8888 or email us at info@etiqa.com.my.<br><br>";

			message += "Thank you.<br><br>";
			message += "Yours Sincerely,<br>";
			if (pollicypassword.equals("Y")) {
				message += "Etiqa Life Insurance Berhad<br><br>";
			} else {
				message += "Etiqa Life Insurance Berhad<br><br>";
			}

			String subject = "Etiqa e-Medical Pass e-Policy Password";


			String host = activeDirMailProp.getProperty("emailhost");
			String fromemail = activeDirMailProp.getProperty("from");
			String port = activeDirMailProp.getProperty("emailport");


			Properties properties = new Properties();
			properties.put("mail.smtp.auth", "false");
			properties.put("mail.smtp.starttls.enable", "false");
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			properties.put("mail.smtp.ssl.trust", host);

			Session session = Session.getDefaultInstance(properties);
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("myaccount@etiqa.com.my"));
			InternetAddress[] toAddresses = { new InternetAddress(emailtosend) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			// set plain text message
			msg.setContent(message, "text/html");
			// sends the e-mail
			Transport.send(msg);
			logger.info("Successfully to sent email.");
		} catch (Exception ex) {
			logger.error("Failed to sent email.");
			logger.error(ex.getMessage(), ex);
		}
	}
	
	private void loadEmailProps() {
		InputStream infoad = this.getClass().getClassLoader()
				.getResourceAsStream("com/etiqa/cancercare/email/emailconfig.properties");
		try {
			activeDirMailProp.load(infoad);
		} catch (IOException ex) {
			logger.error(ex.getMessage(), ex);
		}
	}

}
