package com.etiqa.medicalpass.mapper;

import com.etiqa.medicalpass.pojo.DspAdmTblAgentProdMap;
import com.etiqa.medicalpass.pojo.DspAdmTblAgentProdMapExample;
import java.util.List;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface DspAdmTblAgentProdMapMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_ADM_TBL_AGENT_PROD_MAP
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @SelectProvider(type=DspAdmTblAgentProdMapSqlProvider.class, method="countByExample")
    long countByExample(DspAdmTblAgentProdMapExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_ADM_TBL_AGENT_PROD_MAP
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @DeleteProvider(type=DspAdmTblAgentProdMapSqlProvider.class, method="deleteByExample")
    int deleteByExample(DspAdmTblAgentProdMapExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_ADM_TBL_AGENT_PROD_MAP
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @Insert({
        "insert into DSP_ADM_TBL_AGENT_PROD_MAP (AGP_ID, AGENT_CODE, ",
        "PRODUCT_CODE, AGENT_TYPE, ",
        "AGENT_ENTITY, AGENT_NAME, ",
        "DISCOUNT, COMMISSION, ",
        "PROD_REF_URL, AGENT_UUID, ",
        "ADDON_SET_ID)",
        "values (#{agpId,jdbcType=DECIMAL}, #{agentCode,jdbcType=VARCHAR}, ",
        "#{productCode,jdbcType=VARCHAR}, #{agentType,jdbcType=VARCHAR}, ",
        "#{agentEntity,jdbcType=VARCHAR}, #{agentName,jdbcType=VARCHAR}, ",
        "#{discount,jdbcType=VARCHAR}, #{commission,jdbcType=VARCHAR}, ",
        "#{prodRefUrl,jdbcType=VARCHAR}, #{agentUuid,jdbcType=VARCHAR}, ",
        "#{addonSetId,jdbcType=DECIMAL})"
    })
    int insert(DspAdmTblAgentProdMap record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_ADM_TBL_AGENT_PROD_MAP
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @InsertProvider(type=DspAdmTblAgentProdMapSqlProvider.class, method="insertSelective")
    int insertSelective(DspAdmTblAgentProdMap record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_ADM_TBL_AGENT_PROD_MAP
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @SelectProvider(type=DspAdmTblAgentProdMapSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="AGP_ID", property="agpId", jdbcType=JdbcType.DECIMAL),
        @Result(column="AGENT_CODE", property="agentCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="PRODUCT_CODE", property="productCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="AGENT_TYPE", property="agentType", jdbcType=JdbcType.VARCHAR),
        @Result(column="AGENT_ENTITY", property="agentEntity", jdbcType=JdbcType.VARCHAR),
        @Result(column="AGENT_NAME", property="agentName", jdbcType=JdbcType.VARCHAR),
        @Result(column="DISCOUNT", property="discount", jdbcType=JdbcType.VARCHAR),
        @Result(column="COMMISSION", property="commission", jdbcType=JdbcType.VARCHAR),
        @Result(column="PROD_REF_URL", property="prodRefUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="AGENT_UUID", property="agentUuid", jdbcType=JdbcType.VARCHAR),
        @Result(column="ADDON_SET_ID", property="addonSetId", jdbcType=JdbcType.DECIMAL)
    })
    List<DspAdmTblAgentProdMap> selectByExample(DspAdmTblAgentProdMapExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_ADM_TBL_AGENT_PROD_MAP
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @UpdateProvider(type=DspAdmTblAgentProdMapSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") DspAdmTblAgentProdMap record, @Param("example") DspAdmTblAgentProdMapExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table DSP_ADM_TBL_AGENT_PROD_MAP
     *
     * @mbg.generated Wed Jan 30 15:41:57 SGT 2019
     */
    @UpdateProvider(type=DspAdmTblAgentProdMapSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") DspAdmTblAgentProdMap record, @Param("example") DspAdmTblAgentProdMapExample example);
}