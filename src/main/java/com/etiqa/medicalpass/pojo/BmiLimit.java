package com.etiqa.medicalpass.pojo;

public class BmiLimit {

	private Integer ageUpper;

	private Integer ageLower;

	private Integer bmiUpper;

	private Integer bmiLower;

	public Integer getAgeUpper() {
		return ageUpper;
	}

	public void setAgeUpper(Integer ageUpper) {
		this.ageUpper = ageUpper;
	}

	public Integer getAgeLower() {
		return ageLower;
	}

	public void setAgeLower(Integer ageLower) {
		this.ageLower = ageLower;
	}

	public Integer getBmiUpper() {
		return bmiUpper;
	}

	public void setBmiUpper(Integer bmiUpper) {
		this.bmiUpper = bmiUpper;
	}

	public Integer getBmiLower() {
		return bmiLower;
	}

	public void setBmiLower(Integer bmiLower) {
		this.bmiLower = bmiLower;
	}
}
