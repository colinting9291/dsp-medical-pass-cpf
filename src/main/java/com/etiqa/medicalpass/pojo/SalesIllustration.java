package com.etiqa.medicalpass.pojo;

public class SalesIllustration {

	private String endPolicyYear;
	
	private String endCoveredAge;

	private String totalPremiumToDate;
	
	private String annualPremium;
	
	private String wakalahFee;

	public String getEndPolicyYear() {
		return endPolicyYear;
	}

	public void setEndPolicyYear(String endPolicyYear) {
		this.endPolicyYear = endPolicyYear == null ? null : endPolicyYear.trim();
	}

	public String getEndCoveredAge() {
		return endCoveredAge;
	}

	public void setEndCoveredAge(String endCoveredAge) {
		this.endCoveredAge = endCoveredAge == null ? null : endCoveredAge.trim();
	}

	public String getTotalPremiumToDate() {
		return totalPremiumToDate;
	}

	public void setTotalPremiumToDate(String totalPremiumToDate) {
		this.totalPremiumToDate = totalPremiumToDate == null ? null : totalPremiumToDate.trim();
	}

	public String getAnnualPremium() {
		return annualPremium;
	}

	public void setAnnualPremium(String annualPremium) {
		this.annualPremium = annualPremium == null ? null : annualPremium.trim();
	}

	public String getWakalahFee() {
		return wakalahFee;
	}

	public void setWakalahFee(String wakalahFee) {
		this.wakalahFee = wakalahFee == null ? null : wakalahFee.trim();
	}
	
	
	
}
