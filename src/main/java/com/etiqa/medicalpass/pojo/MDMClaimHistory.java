package com.etiqa.medicalpass.pojo;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class MDMClaimHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	private String claimReferenceNumber;

    private String productCompany;

    private String policyCertificateNumber;

    private String sourceSystem;

    private String claimOn;

    private String responseCode;

	@SerializedName("trx_id")
    private String trxId;

    private String claimType;

    private String lossType;

    private String lossTypeCode;

    private String claimDate;

    private String policyType;

    private String claimApproveAmount;

    private String claimStatus;

    private String eventDate;

	public String getClaimReferenceNumber() {
		return claimReferenceNumber;
	}

	public void setClaimReferenceNumber(String claimReferenceNumber) {
		this.claimReferenceNumber = claimReferenceNumber == null ? null : claimReferenceNumber.trim();
	}

	public String getProductCompany() {
		return productCompany;
	}

	public void setProductCompany(String productCompany) {
		this.productCompany = productCompany == null ? null : productCompany.trim();
	}

	public String getPolicyCertificateNumber() {
		return policyCertificateNumber;
	}

	public void setPolicyCertificateNumber(String policyCertificateNumber) {
		this.policyCertificateNumber = policyCertificateNumber == null ? null : policyCertificateNumber.trim();
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem == null ? null : sourceSystem.trim();
	}

	public String getClaimOn() {
		return claimOn;
	}

	public void setClaimOn(String claimOn) {
		this.claimOn = claimOn == null ? null : claimOn.trim();
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode == null ? null : responseCode.trim();
	}

	public String getTrxId() {
		return trxId;
	}

	public void setTrxId(String trxId) {
		this.trxId = trxId == null ? null : trxId.trim();
	}

	public String getClaimType() {
		return claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType == null ? null : claimType.trim();
	}

	public String getLossType() {
		return lossType;
	}

	public void setLossType(String lossType) {
		this.lossType = lossType == null ? null : lossType.trim();
	}

	public String getLossTypeCode() {
		return lossTypeCode;
	}

	public void setLossTypeCode(String lossTypeCode) {
		this.lossTypeCode = lossTypeCode == null ? null : lossTypeCode.trim();
	}

	public String getClaimDate() {
		return claimDate;
	}

	public void setClaimDate(String claimDate) {
		this.claimDate = claimDate == null ? null : claimDate.trim();
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType == null ? null : policyType.trim();
	}

	public String getClaimApproveAmount() {
		return claimApproveAmount;
	}

	public void setClaimApproveAmount(String claimApproveAmount) {
		this.claimApproveAmount = claimApproveAmount == null ? null : claimApproveAmount.trim();
	}

	public String getClaimStatus() {
		return claimStatus;
	}

	public void setClaimStatus(String claimStatus) {
		this.claimStatus = claimStatus == null ? null : claimStatus.trim();
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate == null ? null : eventDate.trim();
	}
    
    
}
