package com.etiqa.medicalpass.constant;

public class DocumentConstant {
	
	public static final String PDF_POSTFIX = ".pdf";

	public static final String ERROR_MESSAGE_DOCUMENT_NOT_FOUND = "Document {} does not exist.";

	public static final String DOCUMENT_NAME_DRAFT_APPLICATION_FORM = "EDraftAppForm";
	public static final String DOCUMENT_NAME_APPLICATION_FORM = "ApplicationForm";
	public static final String DOCUMENT_NAME_POLICY_INFROMATION_PAGE = "PolicyInformationPage";
	public static final String DOCUMENT_NAME_CLAIMS_GUIDE = "ClaimsGuide";
	public static final String DOCUMENT_NAME_RECEIPT_FROM = "Receipt";
	public static final String DOCUMENT_NAME_CONTRACT = "Contract";
	public static final String DOCUMENT_NAME_NOMINATION = "Nomination";
	public static final String DOCUMENT_NAME_PDS = "PDS";
	public static final String DOCUMENT_NAME_SALES_ILLUSTRATION = "SI";



	public static final String DOCUMENT_DESTINATION_PATH_KEY = "DocumentDestionationPath";

}
