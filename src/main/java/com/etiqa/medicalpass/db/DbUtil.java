package com.etiqa.medicalpass.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DbUtil {

	private static final Logger logger = LoggerFactory.getLogger(DbUtil.class);

	public Connection getConnection() {
		Context ctx = null;
		Connection conn = null;
		try {

			ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("jdbc/dspuser");
			conn = ds.getConnection();

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return conn;

	}

	public static void close(ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

	}

	public static void close(CallableStatement rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

	}

	public static void close(Connection rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

	}
}
