package com.etiqa.medicalpass.service.impl;

import java.io.IOException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.mapper.DspCommonTblCustomerMapper;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomer;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomerExample;
import com.etiqa.medicalpass.service.DspCommonTblCustomerService;

@Service
public class DspCommonTblCustomerServiceImpl implements DspCommonTblCustomerService {

	private static final Logger logger = LoggerFactory.getLogger(DspCommonTblCustomerServiceImpl.class);

	@Autowired
	private DspCommonTblCustomerMapper mapper;

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Integer> insertDspCommonTblCustomer(DspCommonTblCustomer record) throws IOException {
		Integer count = 0;
		try {
			count = mapper.insertSelective(record);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<Integer>(count);
	}
	
	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Integer> updateDspCommonTblCustomer(DspCommonTblCustomer record, DspCommonTblCustomerExample example) throws IOException {
		Integer count = 0;
		try {
			count = mapper.updateByExampleSelective(record, example);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<Integer>(count);
	}

}
