package com.etiqa.medicalpass.service.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.mapper.DspMiTblPostcodesMapper;
import com.etiqa.medicalpass.pojo.DspMiTblPostcodes;
import com.etiqa.medicalpass.pojo.DspMiTblPostcodesExample;
import com.etiqa.medicalpass.service.DspMiTblPostcodesService;

@Service
public class DspMiTblPostcodesServiceImpl implements DspMiTblPostcodesService {

	private static final Logger logger = LoggerFactory.getLogger(DspMiTblPostcodesServiceImpl.class);

	@Autowired
	private DspMiTblPostcodesMapper mapper;

	@Override
	public List<DspMiTblPostcodes> getState(HttpServletRequest request, String postcode) throws IOException {

		List<DspMiTblPostcodes> dspMiTblPostcodes = null;
		try {
			DspMiTblPostcodesExample example = new DspMiTblPostcodesExample();
			example.createCriteria().andPostcodeEqualTo(postcode);
			dspMiTblPostcodes = mapper.selectByExample(example);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}

		return dspMiTblPostcodes;
	}

}
