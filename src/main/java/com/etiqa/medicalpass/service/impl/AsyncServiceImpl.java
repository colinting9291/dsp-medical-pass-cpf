package com.etiqa.medicalpass.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.ws.Dispatch;
import javax.xml.ws.soap.SOAPBinding;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.pojo.MDMClaimHistory;
import com.etiqa.medicalpass.service.AsyncService;
import com.etiqa.medicalpass.util.CommonUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

@Service
public class AsyncServiceImpl implements AsyncService {

	private static final Logger logger = LoggerFactory.getLogger(AsyncServiceImpl.class);

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Map<String, Object>> getMDMCustomerService(String nric, String url) throws IOException {
		logger.info("URL: {}", url);
		Map<String, Object> customer = new HashMap<String, Object>();
		try {
			QName servicename = new QName("http://absec.com/", "Get_Dsp_Customer_ProfileService");
			QName portname = new QName("http://absec.com/", "Get_Dsp_Customer_ProfilePort");
			javax.xml.ws.Service service = javax.xml.ws.Service.create(null, servicename);
			service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING, url + "Get_Dsp_Customer_ProfileService");
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
					javax.xml.ws.Service.Mode.MESSAGE);
			MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
			SOAPMessage request = mf.createMessage();
			SOAPPart part = request.getSOAPPart();
			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();
			SOAPElement operation = body.addChildElement("getDspCustomerProfile", "abs", "http://absec.com/");
			SOAPElement elementUsername = operation.addChildElement("username");
			elementUsername.addTextNode("admin");
			SOAPElement elementPassword = operation.addChildElement("password");
			elementPassword.addTextNode("");
			SOAPElement elementNricNumber = operation.addChildElement("ICNumber");
			elementNricNumber.addTextNode(nric);
			SOAPElement elementTrxId = operation.addChildElement("TrxId");
			elementTrxId.addTextNode("");
			request.saveChanges();
			CommonUtils.writeToBaos(request);

			SOAPMessage response = dispatch.invoke(request);
			SOAPBody responsebody = response.getSOAPBody();
			Iterator<?> custInfo = responsebody.getChildElements();
			while (custInfo.hasNext()) {
				SOAPElement cust = (SOAPElement) custInfo.next();
				Iterator<?> i = cust.getChildElements();
				while (i.hasNext()) {
					SOAPElement e = (SOAPElement) i.next();
					Iterator<?> m = e.getChildElements();
					while (m.hasNext()) {
						SOAPElement e2 = (SOAPElement) m.next();
						customer.put(e2.getLocalName(), e2.getValue());
					}
				}
			}
			logger.info("Result: {}", customer);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<Map<String, Object>>(customer);
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Integer> getMDMClaimHistoryService(String nric, String url) throws IOException {
		logger.info("URL: {}", url);
		Map<String, Object> customer = new HashMap<String, Object>();
		List<MDMClaimHistory> claimHistories = new ArrayList<>();
		try {
			QName servicename = new QName("http://absec.com/", "MyClaimHistoryGetService");
			QName portname = new QName("http://absec.com/", "MyClaimHistoryGetService");
			javax.xml.ws.Service service = javax.xml.ws.Service.create(null, servicename);
			service.addPort(portname, SOAPBinding.SOAP11HTTP_BINDING, url + "MyClaimHistoryGetService");
			Dispatch<SOAPMessage> dispatch = service.createDispatch(portname, SOAPMessage.class,
					javax.xml.ws.Service.Mode.MESSAGE);
			MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
			SOAPMessage request = mf.createMessage();
			SOAPPart part = request.getSOAPPart();
			SOAPEnvelope env = part.getEnvelope();
			SOAPBody body = env.getBody();
			SOAPElement operation = body.addChildElement("getmyclaimhistory", "ws", "http://absec.com/");
			SOAPElement elementUsername = operation.addChildElement("username");
			elementUsername.addTextNode("admin");
			SOAPElement elementPassword = operation.addChildElement("password");
			elementPassword.addTextNode("");
			SOAPElement elementNricNumber = operation.addChildElement("ICNumber");
			elementNricNumber.addTextNode(nric);
			SOAPElement elementTrxId = operation.addChildElement("TrxId");
			elementTrxId.addTextNode("");
			request.saveChanges();
			CommonUtils.writeToBaos(request);

			SOAPMessage response = dispatch.invoke(request);
			SOAPBody responsebody = response.getSOAPBody();
			Iterator<?> custInfo = responsebody.getChildElements();
			MDMClaimHistory claimHistory = null;
			Gson gson = new Gson();
			JsonElement jsonElement = null;
			while (custInfo.hasNext()) {
				SOAPElement cust = (SOAPElement) custInfo.next();
				Iterator<?> i = cust.getChildElements();
				while (i.hasNext()) {
					SOAPElement e = (SOAPElement) i.next();
					Iterator<?> m = e.getChildElements();
					while (m.hasNext()) {
						SOAPElement e2 = (SOAPElement) m.next();
						customer.put(e2.getLocalName(), e2.getValue());
					}
					jsonElement = gson.toJsonTree(customer);
					claimHistory = gson.fromJson(jsonElement, MDMClaimHistory.class);
					if (claimHistory.getClaimReferenceNumber() != null) {
						claimHistories.add(claimHistory);
					}
				}
			}
			logger.info("Result: {}", customer);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<Integer>(claimHistories.size());
	}

}
