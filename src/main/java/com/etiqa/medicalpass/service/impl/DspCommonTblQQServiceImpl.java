package com.etiqa.medicalpass.service.impl;

import java.io.IOException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.etiqa.medicalpass.mapper.DspCommonTblQQMapper;
import com.etiqa.medicalpass.pojo.DspCommonTblQQ;
import com.etiqa.medicalpass.pojo.DspCommonTblQQExample;
import com.etiqa.medicalpass.service.DspCommonTblQQService;

@Service
public class DspCommonTblQQServiceImpl implements DspCommonTblQQService {

	private static final Logger logger = LoggerFactory.getLogger(DspCommonTblQQServiceImpl.class);

	@Autowired
	private DspCommonTblQQMapper mapper;

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Integer> insertDspCommonTblQQ(DspCommonTblQQ record) throws IOException {
		Integer count = 0;
		try {
			count = mapper.insertSelective(record);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<Integer>(count);
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public Future<Integer> updateDspCommonTblQQ(DspCommonTblQQ record, DspCommonTblQQExample example)
			throws IOException {
		Integer count = 0;
		try {
			count = mapper.updateByExampleSelective(record, example);
			logger.info("Result: {}", count);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			Thread.currentThread().interrupt();
		}

		return new AsyncResult<Integer>(count);
	}
}
