package com.etiqa.medicalpass.service;

import java.io.IOException;
import java.util.concurrent.Future;

import com.etiqa.medicalpass.pojo.DspCommonTblQQ;
import com.etiqa.medicalpass.pojo.DspCommonTblQQExample;

public interface DspCommonTblQQService {

	public Future<Integer> insertDspCommonTblQQ(DspCommonTblQQ record) throws IOException;

	public Future<Integer> updateDspCommonTblQQ(DspCommonTblQQ record, DspCommonTblQQExample example)
			throws IOException;

}
