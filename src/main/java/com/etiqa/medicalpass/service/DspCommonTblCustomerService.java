package com.etiqa.medicalpass.service;

import java.io.IOException;
import java.util.concurrent.Future;

import com.etiqa.medicalpass.pojo.DspCommonTblCustomer;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomerExample;

public interface DspCommonTblCustomerService {

	public Future<Integer> insertDspCommonTblCustomer(DspCommonTblCustomer record) throws IOException;

	public Future<Integer> updateDspCommonTblCustomer(DspCommonTblCustomer record, DspCommonTblCustomerExample example)
			throws IOException;
}
