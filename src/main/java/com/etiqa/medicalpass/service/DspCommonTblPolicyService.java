package com.etiqa.medicalpass.service;

import java.io.IOException;
import java.util.List;

import com.etiqa.medicalpass.pojo.DspCommonTblPolicy;
import com.etiqa.medicalpass.pojo.DspCommonTblPolicyExample;

public interface DspCommonTblPolicyService {

	public List<DspCommonTblPolicy> getPolicyDetails(DspCommonTblPolicy record, DspCommonTblPolicyExample example)
			throws IOException;

}
