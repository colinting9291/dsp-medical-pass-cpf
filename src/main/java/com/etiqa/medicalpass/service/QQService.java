package com.etiqa.medicalpass.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface QQService {

	public Map<String, Object> calculateBMI(HttpServletRequest request, String weight, String height);

}
