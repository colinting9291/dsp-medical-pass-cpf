package com.etiqa.medicalpass.service;

import java.io.IOException;

import com.etiqa.medicalpass.pojo.DspMpTblCreditCardOwner;

public interface DspMpTblCreditCardOwnerService {

	public void insertDspMpTblCreditCardOwnerVoidAsync(DspMpTblCreditCardOwner record) throws IOException;

}
