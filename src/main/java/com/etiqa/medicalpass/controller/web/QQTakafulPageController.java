package com.etiqa.medicalpass.controller.web;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.etiqa.medicalpass.constant.QQConstant;
import com.etiqa.medicalpass.pojo.DspCommonTblBankList;
import com.etiqa.medicalpass.pojo.DspCommonTblCustomer;
import com.etiqa.medicalpass.pojo.DspMpTblQQ;
import com.etiqa.medicalpass.pojo.PaymentResponse;
import com.etiqa.medicalpass.pojo.SalesIllustration;
import com.etiqa.medicalpass.pojo.VpmsRequest;
import com.etiqa.medicalpass.pojo.VpmsResponseData;
import com.etiqa.medicalpass.util.CommonUtils;

@Controller
@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_TAKAFUL_PREFIX + QQConstant.REQUEST_MAPPING_ENGLISH)
public class QQTakafulPageController {

	private static final Logger logger = LoggerFactory.getLogger(QQTakafulPageController.class);

	@Value("#{servletContext.contextPath}")
	private String servletContextPath;

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_ONE, method = RequestMethod.GET)
	public String qq1(Model model, HttpSession session) {
		boolean isRejected = session.getAttribute(QQConstant.IS_REJECTED_SESSION_KEY) == null ? false
				: ((boolean) session.getAttribute(QQConstant.IS_REJECTED_SESSION_KEY));
		if (isRejected == false) {
			model.addAttribute("today", CommonUtils.formatVPMSDate());
			CommonUtils.clearAllSessionExceptRejected(session);
		}
		model.addAttribute(QQConstant.MODAL_KEY_IS_REJECTED, isRejected);
		CommonUtils.clearSession(QQConstant.MODAL_KEY_IS_REJECTED, session);
		session.setAttribute(QQConstant.COMPANY_SESSION_KEY, QQConstant.TAKAFUL_COMPANY_KEY);
		model.addAttribute("company", QQConstant.TAKAFUL_COMPANY_KEY);

		return QQConstant.PAGE_DIRECTORY_TAKAFUL_QQ1_EN;
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_TWO_A, method = RequestMethod.GET)
	public String qq2a(Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			HttpSession session = request.getSession();
			DspMpTblQQ mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			VpmsRequest vpmsRequest = (VpmsRequest) session.getAttribute(QQConstant.VPMS_REQUEST_SESSION_KEY);
			VpmsResponseData vpmsResponseData = (VpmsResponseData) session
					.getAttribute(QQConstant.VPMS_RESPONSE_SESSION_KEY);

			String company = (String) session.getAttribute(QQConstant.COMPANY_SESSION_KEY);
			CommonUtils.checkCompanyBySession(response, company, QQConstant.TAKAFUL_COMPANY_KEY);

			boolean isRejected = session.getAttribute(QQConstant.IS_REJECTED_SESSION_KEY) == null ? false
					: ((boolean) session.getAttribute(QQConstant.IS_REJECTED_SESSION_KEY));
			if (mpQQData == null || qqIdKey == null || isRejected == true) {
				response.sendRedirect(servletContextPath + QQConstant.PAGE_URL_TAKAFUL_QQ1_EN);
			} else {
				model.addAttribute(QQConstant.MODAL_KEY_PREMIUM_FREQUENCY_TYPE,
						StringUtils.capitalize(mpQQData.getPremiumPaymentFrequencyType()));
				model.addAttribute(QQConstant.MODAL_KEY_PREMIUM_FREQUENCY,
						CommonUtils.formatPremium(mpQQData.getPremiumPaymentFrequency()));
				model.addAttribute(QQConstant.MODAL_KEY_PLAN_TYPE,
						CommonUtils.getPlanNameFromProductCode(mpQQData.getProductCode()));
				model.addAttribute(QQConstant.MODAL_KEY_ANNUAL_LIMIT, CommonUtils.formatCoverage(
						StringUtils.replace(vpmsResponseData.getAnnualLimit(), QQConstant.VPMS_RINGGIT_MALAYSIA, "")));
				model.addAttribute(QQConstant.MODAL_KEY_DEDUCTIBLE,
						CommonUtils.formatCoverage(vpmsRequest.getDeductible()));
				model.addAttribute(QQConstant.MODAL_KEY_ROOM_RATE, CommonUtils.formatCoverage(StringUtils
						.replace(vpmsResponseData.getRoomRatePerDay(), QQConstant.VPMS_RINGGIT_MALAYSIA, "")));
				model.addAttribute(QQConstant.MODAL_KEY_IS_REJECTED, isRejected);
				CommonUtils.clearSession(QQConstant.MODAL_KEY_IS_REJECTED, session);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return QQConstant.PAGE_DIRECTORY_TAKAFUL_QQ2A_EN;
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_TWO_B, method = RequestMethod.GET)
	public String qq2b(Model model, HttpServletRequest request, HttpServletResponse response) {
		try {
			HttpSession session = request.getSession();
			DspMpTblQQ mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			DspCommonTblCustomer dspCommonTblCustomer = (DspCommonTblCustomer) session
					.getAttribute(QQConstant.COMMON_CUSTOMER_DATA_SESSION_KEY);

			VpmsRequest vpmsRequest = (VpmsRequest) session.getAttribute(QQConstant.VPMS_REQUEST_SESSION_KEY);
			VpmsResponseData vpmsResponseData = (VpmsResponseData) session
					.getAttribute(QQConstant.VPMS_RESPONSE_SESSION_KEY);

			String company = (String) session.getAttribute(QQConstant.COMPANY_SESSION_KEY);
			CommonUtils.checkCompanyBySession(response, company, QQConstant.TAKAFUL_COMPANY_KEY);

			boolean isRejected = session.getAttribute(QQConstant.IS_REJECTED_SESSION_KEY) == null ? false
					: ((boolean) session.getAttribute(QQConstant.IS_REJECTED_SESSION_KEY));
			if (mpQQData == null || qqIdKey == null || dspCommonTblCustomer == null || isRejected == true) {
				response.sendRedirect(servletContextPath + QQConstant.PAGE_URL_TAKAFUL_QQ1_EN);
			} else {
				model.addAttribute("name", dspCommonTblCustomer.getCustomerName());
				model.addAttribute("nric", dspCommonTblCustomer.getCustomerNricId());
				model.addAttribute("address1", dspCommonTblCustomer.getCustomerAddress1());
				model.addAttribute("address2", dspCommonTblCustomer.getCustomerAddress2());
				model.addAttribute("address3", dspCommonTblCustomer.getCustomerAddress3());
				model.addAttribute("race", dspCommonTblCustomer.getCustomerRace());
				model.addAttribute("religion", dspCommonTblCustomer.getCustomerReligion());
				model.addAttribute("salary", dspCommonTblCustomer.getCustomerSalaryRange());
				model.addAttribute("email", dspCommonTblCustomer.getCustomerEmail());
				model.addAttribute("mobile", dspCommonTblCustomer.getCustomerMobileNo());
				model.addAttribute("employerName", dspCommonTblCustomer.getCustomerEmployer());
				model.addAttribute("postcode", dspCommonTblCustomer.getCustomerPostcode());
				model.addAttribute("nationality", dspCommonTblCustomer.getCustomerNationality());
				model.addAttribute("isMail", dspCommonTblCustomer.getHomeMailCheck());

				model.addAttribute("address1Mail", dspCommonTblCustomer.getCustomerMailAddress1());
				model.addAttribute("address2Mail", dspCommonTblCustomer.getCustomerMailAddress2());
				model.addAttribute("address3Mail", dspCommonTblCustomer.getCustomerMailAddress3());
				model.addAttribute("postcodeMail", dspCommonTblCustomer.getCustomerMailPostcode());

				model.addAttribute(QQConstant.MODAL_KEY_PREMIUM_FREQUENCY_TYPE,
						StringUtils.capitalize(mpQQData.getPremiumPaymentFrequencyType()));
				model.addAttribute(QQConstant.MODAL_KEY_PREMIUM_FREQUENCY,
						CommonUtils.formatPremium(mpQQData.getPremiumPaymentFrequency()));
				model.addAttribute(QQConstant.MODAL_KEY_PLAN_TYPE,
						CommonUtils.getPlanNameFromProductCode(mpQQData.getProductCode()));
				model.addAttribute(QQConstant.MODAL_KEY_ANNUAL_LIMIT, CommonUtils.formatCoverage(
						StringUtils.replace(vpmsResponseData.getAnnualLimit(), QQConstant.VPMS_RINGGIT_MALAYSIA, "")));
				model.addAttribute(QQConstant.MODAL_KEY_DEDUCTIBLE,
						CommonUtils.formatCoverage(vpmsRequest.getDeductible()));
				model.addAttribute(QQConstant.MODAL_KEY_ROOM_RATE, CommonUtils.formatCoverage(StringUtils
						.replace(vpmsResponseData.getRoomRatePerDay(), QQConstant.VPMS_RINGGIT_MALAYSIA, "")));
			}
			model.addAttribute(QQConstant.MODAL_KEY_IS_REJECTED, isRejected);
			CommonUtils.clearSession(QQConstant.MODAL_KEY_IS_REJECTED, session);

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return QQConstant.PAGE_DIRECTORY_TAKAFUL_QQ2B_EN;
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_THREE, method = RequestMethod.GET)
	public String qq3(HttpServletRequest request, Model model, HttpServletResponse response) {
		try {
			HttpSession session = request.getSession();
			DspMpTblQQ mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			BigDecimal qqIdKey = (BigDecimal) session.getAttribute(QQConstant.DSP_QQ_ID_DATA_SESSION_KEY);
			VpmsRequest vpmsRequest = (VpmsRequest) session.getAttribute(QQConstant.VPMS_REQUEST_SESSION_KEY);
			VpmsResponseData vpmsResponseData = (VpmsResponseData) session
					.getAttribute(QQConstant.VPMS_RESPONSE_SESSION_KEY);
			DspCommonTblCustomer dspCommonTblCustomer = (DspCommonTblCustomer) session
					.getAttribute(QQConstant.COMMON_CUSTOMER_DATA_SESSION_KEY);
			
			String company = (String) session.getAttribute(QQConstant.COMPANY_SESSION_KEY);
			CommonUtils.checkCompanyBySession(response, company, QQConstant.TAKAFUL_COMPANY_KEY);

			boolean isRejected = session.getAttribute(QQConstant.IS_REJECTED_SESSION_KEY) == null ? false
					: ((boolean) session.getAttribute(QQConstant.IS_REJECTED_SESSION_KEY));
			if (mpQQData == null || qqIdKey == null || isRejected == true) {
				response.sendRedirect(servletContextPath + QQConstant.PAGE_URL_TAKAFUL_QQ1_EN);
			} else {
				List<SalesIllustration> salesIllustrations = new ArrayList<>();
				int endCoveredAgeAndWakalahLength = vpmsResponseData.getEndCoveredAgeAndWakalahArray().length;
				int annualPremiumArrayLength = vpmsResponseData.getAnnualPremiumArray().length;
				int endCoveredAgeArrayLength = vpmsResponseData.getEndCoveredAgeArray().length;
				int totalPremiumLength = vpmsResponseData.getTotalPremiumToDate().length;

				boolean isSame = endCoveredAgeAndWakalahLength == annualPremiumArrayLength
						&& endCoveredAgeAndWakalahLength == endCoveredAgeArrayLength 
						&& endCoveredAgeAndWakalahLength == totalPremiumLength;
						
				if (isSame == true) {
					int listSize = endCoveredAgeAndWakalahLength;
					SalesIllustration salesIllustration = null;
					String[] annualPremium = vpmsResponseData.getAnnualPremiumArray();
					String[] totalPremiumToDate = vpmsResponseData.getTotalPremiumToDate();
					String[] endCoveredAge = vpmsResponseData.getEndCoveredAgeArray();				
					String[] wakalahFee = vpmsResponseData.getEndCoveredAgeAndWakalahArray();

					int endPolicyYear = 0;
					for (int i = 0; i < listSize; i++) {
						salesIllustration = new SalesIllustration();
						endPolicyYear = i + 1;
						if (endPolicyYear < 20 || (endPolicyYear >= 20 && endPolicyYear % 5 == 0)
								|| (i == (listSize - 1) && endPolicyYear % 5 != 0)) {
							salesIllustration.setEndPolicyYear(String.valueOf(endPolicyYear));
							salesIllustration.setAnnualPremium(CommonUtils.formatCoverage(annualPremium[i]));
							salesIllustration.setWakalahFee(CommonUtils.formatCoverage(wakalahFee[i]));
							salesIllustration.setTotalPremiumToDate(CommonUtils.formatCoverage(totalPremiumToDate[i]));
							salesIllustration.setEndCoveredAge(endCoveredAge[i]);
							salesIllustrations.add(salesIllustration);
						}
					}
					model.addAttribute("salesIllustrations", salesIllustrations);
				}
				model.addAttribute("company", QQConstant.TAKAFUL_COMPANY_KEY);
				model.addAttribute("name", dspCommonTblCustomer.getCustomerName());
				model.addAttribute("gender", vpmsResponseData.getGender());
				model.addAttribute("policyTerm", vpmsResponseData.getPolicyTerm());
				model.addAttribute("paymentTerm", vpmsResponseData.getPaymentTerm());
				
				if ((mpQQData.getPremiumPaymentFrequencyType()).equalsIgnoreCase("annually")) {
					model.addAttribute("paymentFrequencySITitle", "First Year Contribution (RM)");
					model.addAttribute("premiumPaymentFrequencyTypeSI", "Yearly");
				}else{
					model.addAttribute("paymentFrequencySITitle", "First Year Monthly Contribution (RM)");
					model.addAttribute("premiumPaymentFrequencyTypeSI", "Monthly");
				}
				
				model.addAttribute("deductibleSI", CommonUtils.getDeductionAmountFromString(vpmsRequest.getDeductible()));
				model.addAttribute("deductibleOptionSI", CommonUtils.getDeductionOptionString(vpmsRequest.getDeductible()));
				
				model.addAttribute("anb", CommonUtils.getAge(mpQQData.getDob(), QQConstant.DOB_FORM_DATE_FORMAT) + 1);
				model.addAttribute("dob", CommonUtils.formatDobToSiDate(mpQQData.getDob(), QQConstant.DOB_FORM_DATE_FORMAT));
				model.addAttribute("nric", mpQQData.getNewIcCopy());
				model.addAttribute("occupationClass", vpmsResponseData.getOccupationClass());
				model.addAttribute("qoutationDate", CommonUtils.formatSiDate());
				model.addAttribute("generateDate", CommonUtils.formatPdfDateOnly());
				
				model.addAttribute(QQConstant.MODAL_KEY_PREMIUM_FREQUENCY_TYPE + "PDS",
						mpQQData.getPremiumPaymentFrequencyType());

				model.addAttribute(QQConstant.MODAL_KEY_PREMIUM_FREQUENCY_TYPE,
						StringUtils.capitalize(mpQQData.getPremiumPaymentFrequencyType()));
				model.addAttribute(QQConstant.MODAL_KEY_PREMIUM_FREQUENCY,
						CommonUtils.formatPremium(mpQQData.getPremiumPaymentFrequency()));
				model.addAttribute(QQConstant.MODAL_KEY_PLAN_TYPE,
						CommonUtils.getPlanNameFromProductCode(mpQQData.getProductCode()));
				model.addAttribute(QQConstant.MODAL_KEY_ANNUAL_LIMIT, CommonUtils.formatCoverage(
						StringUtils.replace(vpmsResponseData.getAnnualLimit(), QQConstant.VPMS_RINGGIT_MALAYSIA, "")));
				model.addAttribute(QQConstant.MODAL_KEY_DEDUCTIBLE,
						CommonUtils.formatCoverage(vpmsRequest.getDeductible()));
				model.addAttribute(QQConstant.MODAL_KEY_ROOM_RATE, CommonUtils.formatCoverage(StringUtils
						.replace(vpmsResponseData.getRoomRatePerDay(), QQConstant.VPMS_RINGGIT_MALAYSIA, "")));
			}
			model.addAttribute(QQConstant.MODAL_KEY_IS_REJECTED, isRejected);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return QQConstant.PAGE_DIRECTORY_TAKAFUL_QQ3_EN;
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_FOUR_FAILED, method = RequestMethod.GET)
	public String qq4(HttpServletRequest request, Model model, HttpServletResponse response) {
		try {
			HttpSession session = request.getSession();
			PaymentResponse paymentResponse = (PaymentResponse) session
					.getAttribute(QQConstant.PAYMENT_RESPONSE_SESSION_KEY);

			String company = (String) session.getAttribute(QQConstant.COMPANY_SESSION_KEY);
			CommonUtils.checkCompanyBySession(response, company, QQConstant.TAKAFUL_COMPANY_KEY);

			if (paymentResponse == null) {
				response.sendRedirect(servletContextPath + QQConstant.PAGE_URL_TAKAFUL_QQ1_EN);
			} else {
				model.addAttribute(QQConstant.PAYMENT_MODAL_KEY_AMOUNT, paymentResponse.getAmount());
				model.addAttribute(QQConstant.PAYMENT_MODAL_KEY_TRANSACTION_NUMBER, paymentResponse.getPolicyNumber());
				model.addAttribute(QQConstant.PAYMENT_MODAL_KEY_TRANSACTION_DATE_TIME,
						paymentResponse.getTransactionDateTime());
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return QQConstant.PAGE_DIRECTORY_TAKAFUL_QQ4_FAILED_EN;
	}

	@RequestMapping(value = QQConstant.REQUEST_MAPPING_QQ_FOUR_SUCCESS, method = RequestMethod.GET)
	public String qq4success(HttpServletRequest request, Model model, HttpServletResponse response) {
		try {
			HttpSession session = request.getSession();
			PaymentResponse paymentResponse = (PaymentResponse) session
					.getAttribute(QQConstant.PAYMENT_RESPONSE_SESSION_KEY);
			DspMpTblQQ mpQQData = (DspMpTblQQ) session.getAttribute(QQConstant.MP_QQ_DATA_SESSION_KEY);
			DspCommonTblCustomer dspCommonTblCustomer = (DspCommonTblCustomer) session
					.getAttribute(QQConstant.COMMON_CUSTOMER_DATA_SESSION_KEY);
			List<DspCommonTblBankList> bankList = CommonUtils
					.castList(session.getAttribute(QQConstant.BANK_LIST_SESSION_KEY), DspCommonTblBankList.class);

			String company = (String) session.getAttribute(QQConstant.COMPANY_SESSION_KEY);
			CommonUtils.checkCompanyBySession(response, company, QQConstant.TAKAFUL_COMPANY_KEY);

			if (paymentResponse == null || mpQQData == null || dspCommonTblCustomer == null) {
				response.sendRedirect(servletContextPath + QQConstant.PAGE_URL_TAKAFUL_QQ1_EN);
			} else {
				model.addAttribute("name", dspCommonTblCustomer.getCustomerName());
				model.addAttribute("nric", dspCommonTblCustomer.getCustomerNricId());
				model.addAttribute("html", buildHtmlDropdown(bankList));

				model.addAttribute(QQConstant.PAYMENT_MODAL_KEY_AMOUNT, paymentResponse.getAmount());
				model.addAttribute(QQConstant.PAYMENT_MODAL_KEY_TRANSACTION_NUMBER, paymentResponse.getPolicyNumber());
				model.addAttribute(QQConstant.PAYMENT_MODAL_KEY_TRANSACTION_DATE_TIME,
						paymentResponse.getTransactionDateTime());
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return QQConstant.PAGE_DIRECTORY_TAKAFUL_QQ4_SUCCESS_EN;
	}

	private String buildHtmlDropdown(List<DspCommonTblBankList> bankList) {
		StringBuilder sb = new StringBuilder();
		if (bankList != null) {
			for (DspCommonTblBankList bank : bankList) {
				sb.append("<option value='");
				sb.append(bank.getBankKey());
				sb.append("'>");
				sb.append(bank.getDescription());
				sb.append("</option>");
			}
		}
		return sb.toString();
	}
}